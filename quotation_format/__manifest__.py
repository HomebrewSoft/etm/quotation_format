{
    "name": "quotation_format",
    "version": "13.0.0.1.0",
    "author": "HomebrewSoft",
    "website": "https://homebrewsoft.dev",
    "license": "LGPL-3",
    "depends": [
        "sale",
        "sales_cube",
        "product_prototype",
    ],
    "data": [
        # reports
        "reports/quotation_format.xml",
        # views
        "views/sale_order.xml",
    ],
}
