# -*- coding: utf-8 -*-
from odoo import _, api, fields, models


class SaleOrder(models.Model):
    _inherit = "sale.order"

    customer_attended = fields.Char(
        string="Atención a:",
    )
    observations = fields.Text(
        string="Observaciones",
    )
    texto_inicial = fields.Text(
        default="Estimado cliente: Por medio de la presente nos complace saludarle y al mismo tiempo agradecemos la oportunidad que nos brinda. A continuación encontrará el (los) precio(s) de (los) producto(s) con las características que han sido de su interés.",
    )
    current_user = fields.Many2one(
        comodel_name='res.users',
        default=lambda self: self.env.user,
    )

    def _get_functions(self):
        function_comercial = self.env["res.partner"].search(
            [
                ("name", "=", self.partner_id.user_id.name),
            ]
        )
        job = function_comercial.function
        return job

    def _get_functions_sac(self):
        function_comercial = self.env["res.partner"].search(
            [
                ("name", "=", self.partner_id.user_sac_id.name),
            ]
        )
        job = function_comercial.function
        return job
