# -*- coding: utf-8 -*-
from odoo import _, api, fields, models


class MailComposer(models.TransientModel):
    _inherit = "mail.compose.message"

    sale_order_name = fields.Many2one()
